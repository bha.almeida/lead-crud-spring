package br.com.lead.collector.services;

import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTeste {

    @MockBean
    private LeadRepository leadRepository;

    @MockBean
    private ProdutoRepository produtoRepository;

    @MockBean
    private ProdutoService produtoService;

    @Autowired
    private LeadService leadService;

    private Lead lead;
    private Produto produto;

    @BeforeEach
    public void setUp(){
        this.lead = new Lead();
        lead.setId(1);
        lead.setNome("Bruno");
        lead.setTelefone("(11) 1234-1234");
        lead.setCpf("369.001.268-63");
        lead.setEmail("bruno@teste.com");

        this.produto = new Produto();
        produto.setId(1);
        produto.setNome("Melancia");
        produto.setDescricao("Melancia doce");
        produto.setPreco(1.99);

        List<Produto> produtos = Arrays.asList(produto);

        lead.setProdutos(produtos);
    }

    @Test
    public void testarBuscarLeadPorId(){
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        Assertions.assertEquals(lead, leadService.buscarLeadPeloId(1));
    }

    @Test
    public void testarBuscarLeadPorIdQueNaoExiste(){
        Optional<Lead> leadOptional = Optional.empty();
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        Assertions.assertThrows(RuntimeException.class, () -> {leadService.buscarLeadPeloId(1234);});
    }

    @Test
    public void testarSalvarLead(){

        List<Produto> listaProdutos = Arrays.asList(produto);
        lead.setProdutos(listaProdutos);

        Mockito.when(produtoRepository.findAllById(Mockito.anyIterable())).thenReturn(listaProdutos);
        Mockito.when(produtoService.buscarProdutoPorId(Mockito.anyInt())).thenReturn(produto);
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).then(objeto -> objeto.getArgument(0));

        Lead testeDeLead = leadService.salvarLead(this.lead);

        Assertions.assertEquals("Bruno", testeDeLead.getNome());
    }

}
