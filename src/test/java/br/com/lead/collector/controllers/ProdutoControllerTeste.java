package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

@WebMvcTest(ProdutoController.class)
public class ProdutoControllerTeste {

    @MockBean
    private ProdutoService produtoService;

    @Autowired
    private MockMvc mockMvc;

    Produto produto;

    @BeforeEach
    public void setUp(){
        this.produto = new Produto();
        produto.setId(1);
        produto.setNome("Melancia");
        produto.setDescricao("Melancia doce");
        produto.setPreco(1.99);

        List<Produto> produtos = Arrays.asList(produto);
    }

    @Test
    public void testarBuscarProdutoPorId() throws Exception {
        Mockito.when(produtoService.buscarProdutoPorId(Mockito.anyInt())).thenReturn(produto);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarBuscarProdutoPorIdNaoEncontrado() throws Exception {
        Mockito.when(produtoService.buscarProdutoPorId(Mockito.anyInt())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarCadastrarProduto() throws Exception {
        Mockito.when(produtoService.cadastrarProduto(Mockito.any(Produto.class))).thenReturn(produto);

        //usar para converter um objeto em JSON ou ao contrario
        ObjectMapper objectMapper = new ObjectMapper();
        String produtoJson = objectMapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.post("/produtos")
                .contentType(MediaType.APPLICATION_JSON).content(produtoJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Melancia")));
    }

}
