package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.LeadService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebMvcTest(LeadController.class)
public class LeadControllerTeste {

    @MockBean
    private LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    Lead lead;
    Produto produto;

    @BeforeEach
    public void setUp(){
        this.lead = new Lead();
        lead.setId(1);
        lead.setNome("Bruno");
        lead.setTelefone("(11) 1234-1234");
        lead.setCpf("369.001.268-63");
        lead.setEmail("bruno@teste.com");

        this.produto = new Produto();
        produto.setId(1);
        produto.setNome("Melancia");
        produto.setDescricao("Melancia doce");
        produto.setPreco(1.99);

        List<Produto> produtos = Arrays.asList(produto);

        lead.setProdutos(produtos);
    }

    @Test
    public void testarBuscarLeadPorId() throws Exception {
        Mockito.when(leadService.buscarLeadPeloId(Mockito.anyInt())).thenReturn(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarBuscarPorIdQueNaoExiste() throws Exception {
        Mockito.when(leadService.buscarLeadPeloId(Mockito.anyInt())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarLerTodosLeads() throws Exception {
        List<Lead> listaDeLeads = new ArrayList<>();
        Mockito.when(leadService.lerTodosLeads()).thenReturn(listaDeLeads);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray());
    }

    @Test
    public void testarCadastrarLead() throws Exception {
        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).thenReturn(lead);

        //usar para converter um objeto em JSON ou ao contrario
        ObjectMapper objectMapper = new ObjectMapper();
        String leadJson = objectMapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
        .contentType(MediaType.APPLICATION_JSON).content(leadJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Bruno")));
    }

    @Test
    public void testarValidacaoCadastroLead() throws Exception {
        lead.setEmail("@4frfrvfc");
        ObjectMapper objectMapper = new ObjectMapper();
        String leadJson = objectMapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON).content(leadJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

        //verifica se o serviço foi chamado X vezes
        Mockito.verify(leadService, Mockito.times(0))
                .salvarLead(Mockito.any(Lead.class));

    }
}
