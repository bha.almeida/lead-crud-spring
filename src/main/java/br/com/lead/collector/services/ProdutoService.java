package br.com.lead.collector.services;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Produto cadastrarProduto(Produto produto){
        return produtoRepository.save(produto);
    }

    public Iterable<Produto> listarTodosProdutos(){
        return produtoRepository.findAll();
    }

    public Produto buscarProdutoPorId(int id){
        Optional<Produto> optionalProduto = produtoRepository.findById(id);

        if(optionalProduto.isPresent()){
            Produto produto = optionalProduto.get();
            produtoRepository.findById(produto.getId());
            return produto;
        }
        else {
            throw new RuntimeException("Produto não encontrado!");
        }
    }

    public Produto atualizarProduto(Produto produto, int id){
        Produto produtoDB = buscarProdutoPorId(id);
        produto.setId(produtoDB.getId());
        return produtoRepository.save(produto);
    }

    public void deletarProduto(int id){
        Produto produtoDB = buscarProdutoPorId(id);
        produtoRepository.delete(produtoDB);
    }
}
