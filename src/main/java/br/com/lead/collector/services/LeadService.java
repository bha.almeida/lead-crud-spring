package br.com.lead.collector.services;

import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Null;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private ProdutoService produtoService;

    public Lead salvarLead(Lead lead){
        List<Produto> produtos = new ArrayList<>();
        for(Produto produto : lead.getProdutos()){
            int id = produto.getId();
            Produto produtoDB = produtoService.buscarProdutoPorId(id);
            produtos.add(produtoDB);
        }
        lead.setProdutos(produtos);
        return leadRepository.save(lead);
    }

    public Iterable<Lead> lerTodosLeads(){
        return leadRepository.findAll();
    }

    public Lead buscarLeadPeloId(int id){
        Optional<Lead> leadOptional = leadRepository.findById(id);

        if(leadOptional.isPresent()){
            Lead lead = leadOptional.get();
            return lead;
        }
        else {
            throw new RuntimeException("Lead não encontrado!");
        }
    }

    public Lead atualizarLead(int id, Lead lead){
        Lead leadDB = buscarLeadPeloId(id);
        lead.setId(leadDB.getId());
        return leadRepository.save(lead);
    }

    public void deletarLead(int id){
        if(leadRepository.existsById(id)) {
            leadRepository.deleteById(id);
        }
        else {
            throw new RuntimeException("Lead não existe!");
        }
    }

    public Lead pesquisarPorCpf(String cpf){
        Lead lead = leadRepository.findFirstByCpf(cpf);
        if(lead != null){
            return lead;
        } else {
            throw new RuntimeException("CPF não encontrado!");
        }
    }

    public List<Lead> pesquisarPorIdProduto(int id){
        return leadRepository.findByProdutosId(id);
    }
}
