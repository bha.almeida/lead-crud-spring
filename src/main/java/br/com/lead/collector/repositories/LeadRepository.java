package br.com.lead.collector.repositories;

import br.com.lead.collector.models.Lead;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LeadRepository extends CrudRepository<Lead,Integer> {

    //usar método padrão para fazer uma query automaticamente
    Lead findFirstByCpf(String cpf);

    List<Lead> findByProdutosId(int id);

    //sem usar o método padrão construindo na mão a query
//    @Query(value = "SELECT * FROM lead WHERE email = :email")
//    Lead buscarporEmail(String email);


}
