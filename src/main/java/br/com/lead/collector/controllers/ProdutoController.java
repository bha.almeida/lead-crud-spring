package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produto cadastrarProduto(@RequestBody @Valid Produto produto){
        return produtoService.cadastrarProduto(produto);
    }

    @GetMapping
    public Iterable<Produto> listarTodosProdutos(){
        return produtoService.listarTodosProdutos();
    }

    @GetMapping("/{id_produto}")
    public Produto buscarProdutoPorId(@PathVariable(name = "id_produto") int id){
        try {
            return produtoService.buscarProdutoPorId(id);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id_produto}")
    public Produto atualizarProduto(@RequestBody @Valid Produto produto, @PathVariable(name = "id_produto") int id){
        try {
            return produtoService.atualizarProduto(produto, id);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id_produto}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarProduto(@PathVariable(name = "id_produto") int id){
        try{
            produtoService.deletarProduto(id);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
