package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Lead;
import br.com.lead.collector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/leads")
public class LeadController {

    @Autowired
    private LeadService leadService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Lead salvarLead(@RequestBody @Valid Lead lead){
        return leadService.salvarLead(lead);
    }

    @GetMapping
    public Iterable<Lead> lerTodosLeads(){
        return leadService.lerTodosLeads();
    }

    @GetMapping("/{id_lead}")
    public Lead buscarLeadPorId(@PathVariable(name = "id_lead") int id){
        try {
            return leadService.buscarLeadPeloId(id);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id_lead}")
    public Lead atualizarLead(@PathVariable(name="id_lead") int id, @RequestBody @Valid Lead lead){
        try {
            return leadService.atualizarLead(id, lead);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id_lead}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarLead(@PathVariable(name = "id_lead") int id){
        try {
            leadService.deletarLead(id);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/detalhar")
    public Lead detalharLead(@RequestParam(name = "cpf") String cpf){
        try{
            return leadService.pesquisarPorCpf(cpf);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/detalharPorProduto")
    public List<Lead> pesquisarPorIdPorduto(@RequestParam(name = "idProduto") int id){
        try{
            return leadService.pesquisarPorIdProduto(id);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
